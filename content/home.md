---
permalink: /
---
Welcome to the Codeberg Documentation pages!

Please choose a section from the menu on the left.

If you're new to Codeberg, consider reading the [Getting Started Guide](/getting-started).