---
eleventyNavigation:
  key: FirstRepository
  title: Your first Repository
  parent: GettingStarted
  order: 30
---

To create a new repository you need to login into your account on Codeberg.org.

After login you can use one of the two buttons shown in the two following screenshot
to create a new repository:

<picture>
  <source srcset="/assets/images/getting-started/first-repository/create-repo.webp" type="image/webp">
  <img src="/assets/images/getting-started/first-repository/create-repo.png" alt="Creating a Repository">
</picture>

> Please note that it is currently not possible to use git on your local machine to push a fresh repository on Codeberg. (Push-to-Create)



## Connect a local repository to Codeberg
Keep in mind that you first need to create a repository on Codeberg as stated in the section before.

### Optional: Create the local repository
If you don't have an existing git repository on your local machine that you can create one.

```bash
mkdir ~/playground
cd ~/playground
touch README.md
git init
git add README.md
git commit -m "first commit"
```

### Declare Codeberg repository as 'origin'
Your local repository need to know where the remote one is. It is usual and common to name it `origin`. The string `origin` could be described as an alias for the remote repository  on the Codeberg server.

Now you have two protocol variantes to connect: SSH and HTTPS.

**Variant - SSH**
```bash
$ git remote add origin ssh://git@codeberg.org/JohnDoe/playground.git
```
**Variant - HTTPS**
```bash
$ git remote add origin https://codeberg.org/JohnDoe/playground.git
```
